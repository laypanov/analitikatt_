package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.data.Result;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;


public class AuthActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        try{
            EditText EditText_address_server=findViewById(R.id.address_server);

            String address_server = "";
            EditText_address_server.setText(address_server);

            SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);

            Cursor query = db.rawQuery("SELECT * FROM auth_user;", null);
            if(query.moveToFirst()){
                String successfully = query.getString(2);
                if (successfully.equals("1")){
                    db.execSQL("INSERT INTO auth_user_opening (opening_date, sync) VALUES (datetime(), 0)");
                }
            }
            query.close();
            db.close();

            Toast.makeText(getApplicationContext(), "Успешный вход", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, MainActivityLeft.class);
            startActivity(intent);
            finish();

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Необходимо выполнить авторизацию", Toast.LENGTH_LONG).show();
        }

    }

    public Result.Error onClickLogin(View view) {
        JSONArray jsonParam_common = new JSONArray();
        JSONObject jsonObject_common_buffer = new JSONObject();
        JSONObject rootObject = new JSONObject();
        JSONObject jObject = new JSONObject();

        EditText EditText_username=findViewById(R.id.username);
        EditText EditText_address_server=findViewById(R.id.address_server);

        String uuid=EditText_username.getText().toString();
        String address_server=EditText_address_server.getText().toString();

        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);

        try {
            if (uuid.matches("") || address_server.matches("")) {
                Toast.makeText(getApplicationContext(), "Необходимо заполнить", Toast.LENGTH_SHORT).show();
                return null;
            }else {
                jsonObject_common_buffer.put("uuid", uuid);
                jsonObject_common_buffer.put("process", "JSON");
                jsonObject_common_buffer.put("post_url", "http://" + address_server + "/sync/download/json/?get_auth=1");
                jsonParam_common.put(jsonObject_common_buffer);

                rootObject.put("common", jsonParam_common);

                String json_data = new PostImageToServerAsync().execute(rootObject).get();
                Log.v("json_data", String.valueOf(json_data));

                if (json_data.equals("False")){
                    Toast.makeText(getApplicationContext(), "Отсутствует соединение с сервером", Toast.LENGTH_LONG).show();
                }else {
                    jObject = new JSONObject(json_data);

                    String jpersonObj = jObject.getString("auth");
                    if (jpersonObj.equals("True")) {
                        //дроп время входа

                        db.execSQL("CREATE TABLE IF NOT EXISTS auth_user (" +
                                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "uuid VARCHAR, " +
                                "successfully BOOLEAN," +
                                "address_server TEXT)");

                        db.execSQL("CREATE TABLE IF NOT EXISTS auth_user_opening (" +
                                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "opening_date date default CURRENT_TIMESTAMP," +
                                "sync bool)");

                        db.execSQL("CREATE TABLE IF NOT EXISTS auth_user_closing (" +
                                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                "closing_date date default CURRENT_TIMESTAMP," +
                                "sync bool)");


                        db.execSQL("INSERT INTO auth_user (uuid, successfully, address_server) " +
                                "VALUES ('" + uuid + "', '1', '" + address_server + "')");
                        db.close();

                        Toast.makeText(getApplicationContext(), "Успешный вход", Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "Необходимо выполнить синхронизацию", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(this, SyncActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Ошибка входа", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } catch (Exception e) {
            return new Result.Error(new IOException("Ошибка входа", e));
        }
        return null;
    }
}