package com.example.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class ConfigActivity extends AppCompatActivity {
    private TextView EditText_config_UUID;
    private TextView EditText_config_address_server;
    private TextView editText_config_map_distance;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_activity);

        try {
            SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);
            EditText_config_UUID = (TextView) this.findViewById(R.id.editText_config_UUID);
            EditText_config_address_server = (TextView) this.findViewById(R.id.editText_config_address_server);
            editText_config_map_distance = (TextView) this.findViewById(R.id.editText_config_map_distance);
            Cursor query;

            query = db.rawQuery("SELECT map_distance_markers FROM options;", null);
            if (query.moveToFirst()) {
                int map_distance_markers = query.getInt(0);
                editText_config_map_distance.setText(String.valueOf(map_distance_markers));
            }
            query.close();

            query = db.rawQuery("SELECT uuid, address_server  FROM auth_user;", null);
            if (query.moveToFirst()) {
                String uuid = query.getString(0);
                String address_server = query.getString(1);

                EditText_config_UUID.setText(uuid);
                EditText_config_address_server.setText(address_server);
            }
            query.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Непредвиденная ошибка", Toast.LENGTH_LONG).show();
        }
    }

    // сохранить
    public void onClick_config_action_btn(View view) {
        try {
            SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);

            String config_UUID = EditText_config_UUID.getText().toString();
            String config_address_server = EditText_config_address_server.getText().toString();
            String map_distance_markers = editText_config_map_distance.getText().toString();

            db.execSQL("INSERT OR REPLACE INTO options (_id, map_distance_markers) " +
                    "VALUES (1, '" + map_distance_markers + "')");

            db.execSQL("" +
                    "UPDATE auth_user " +
                    "SET uuid='" + config_UUID + "',  address_server='" + config_address_server + "' " +
                    "WHERE _id = 1");
            db.close();
            Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Непредвиденная ошибка", Toast.LENGTH_LONG).show();
        }
        Intent intent = new Intent(this, MainActivityLeft.class);
        this.startActivity(intent);
        this.finishAffinity();

    }

    // очистить БД
    public void onClick_config_action_btn2(View view) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("БД будет полностью очищена");
        builder1.setCancelable(true);

        AlertDialog.Builder builder = builder1.setPositiveButton(
                "Да",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);
                        db.execSQL("DROP TABLE IF EXISTS tt_info;");
                        db.execSQL("DROP TABLE IF EXISTS questionnaires;");
                        db.execSQL("DROP TABLE IF EXISTS PhotoReport;");
                        db.execSQL("DROP TABLE IF EXISTS PhotoConsumer;");
                        db.execSQL("DROP TABLE IF EXISTS PhotoStore;");
                        dialog.cancel();
                        finish();
                        Intent intent = new Intent(getApplicationContext(), SyncActivity.class);
                        startActivity(intent);
                    }
                });

        builder1.setNegativeButton(
                "Нет",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
