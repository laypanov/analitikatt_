package com.example.myapplication;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class PostToServer {

    public StringBuffer postImage(String auth, String rec_id, String post_url, String path, String db_name) throws Exception {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        InputStream inputStream = null;
        StringBuffer response = new StringBuffer();

        try {
            String twoHyphens = "--";
            String boundary = "" + rec_id + "";
            String lineEnd = "\r\n";

            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;

            String filefield = "image";

            String[] q = path.split("/");
            int idx = q.length - 1;

            File file = new File(path);
            FileInputStream fileInputStream = new FileInputStream(file);

            URL url = new URL(post_url);
            //URL url = new URL("http://192.168.88.3:8082/sync/upload/");
            connection = (HttpURLConnection) url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            connection.setRequestProperty("Authorization", auth);
            connection.setRequestProperty("rec-id", rec_id);
            connection.setRequestProperty("db-name", db_name);
            connection.setConnectTimeout(50000);
            connection.setReadTimeout(50000);

            outputStream = new DataOutputStream(connection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);


            outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; filename=\"" + q[idx] + "\"" + lineEnd);
            outputStream.writeBytes("Content-Type: image/jpeg" + lineEnd);
            outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);
            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            inputStream = connection.getInputStream();

            int status = connection.getResponseCode();
            if (status == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                inputStream.close();
                connection.disconnect();
                fileInputStream.close();
                outputStream.flush();
                outputStream.close();

                return response;
            } else {
                return response.append("False");
            }
        } catch (IOException e) {
        }
        return response.append("False");
    }

    public StringBuffer PostJson(String uuid, String post_url, JSONArray JSON) throws Exception {

        StringBuffer response = new StringBuffer();
        try {
            URL url = new URL(post_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Language", "ru-RU");
            conn.setRequestProperty("Authorization", uuid);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setConnectTimeout(50000);
            conn.setReadTimeout(50000);

            Writer writer = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));


            if(JSON != null) {
                writer.write(JSON.toString());
                writer.flush();
                writer.close();
            }

            int status = conn.getResponseCode();
            if (status == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                conn.disconnect();
                return response;
            } else {
                return response.append("False");
            }
        } catch (IOException e) {
        }
        return response.append("False");
    }

}
class PostImageToServerAsync extends AsyncTask<JSONObject, String, String> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(JSONObject... params) {
        try {
            String uuid = "";
            String process = "";
            String post_url = "";

            JSONArray the_json_array = params[0].getJSONArray("common");
            int size = the_json_array.length();
            for (int i = 0; i < size; i++) {
                JSONObject another_json_object = the_json_array.getJSONObject(i);
                uuid = (String) another_json_object.get("uuid");
                process = (String) another_json_object.get("process");
                post_url = (String) another_json_object.get("post_url");
            }

            JSONArray the_json_array2 = null;
            String st = "";
            if (params[0].has("questionnaires")) {
                the_json_array2 = params[0].getJSONArray("questionnaires");
                st = "upload";
            }else if (params[0].has("set_photos")) {
                the_json_array2 = params[0].getJSONArray("set_photos");
                st = "upload";
            }else{
                st = "download";
            }

            if (process.equals("JSON") & st.equals("upload")) {
                StringBuffer response = new PostToServer().PostJson(uuid, post_url, the_json_array2);
                return response.toString();
            }else if (process.equals("JSON") & st.equals("download"))  {
                StringBuffer response = new PostToServer().PostJson(uuid, post_url, null);
                return response.toString();
            }else if (process.equals("IMAGE") & st.equals("upload")){
                JSONObject json_object_pic = the_json_array2.getJSONObject(0);
                String rec_id = (String) json_object_pic.get("rec_id");
                String path = (String) json_object_pic.get("path");
                String db_name = (String) json_object_pic.get("db_name");
                StringBuffer response = new PostToServer().postImage(uuid, rec_id, post_url, path, db_name);
                return response.toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        try {
            JSONObject jObject3 = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

