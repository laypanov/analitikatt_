package com.example.myapplication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

import static android.widget.Toast.LENGTH_LONG;


public class AnketaTTFullActivity extends AppCompatActivity implements LocationListener {
    private static final int CAMERA_REQUEST = 2;

    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final String IMAGE_DIRECTORY = "/AnalitikaTT";
    private String rec_id;
    private int status = 0;
    private String name_table_photo;
    private FusedLocationProviderClient fusedLocationClient;

    int PICK_IMAGE_MULTIPLE = 1;
    String imageEncoded;
    List<String> imagesEncodedList;
    private EditText editTextTAddress;
    private EditText editTextTLatitude;
    private EditText editTextTLongitude;
    private EditText editTextName;
    private EditText editTextTS1;
    private EditText editTextTS2;
    private EditText editTextContact;
    private EditText editTextTPСontactNumber;
    private EditText editTextGk;
    private EditText editTextUl;
    private EditText editTextInn;
    private EditText editTextTAddressD;
    private EditText editTextTPPA;

    private Spinner spinnerFT;
    private Spinner spinnerKk;
    private Spinner spinnerTipTT;
    private Spinner editTextPost;
    private Spinner spinnerKanal;
    private Spinner spinnerSostav;
    private Spinner spinnerPost;

    private ImageView imageViewGetLocate;
    private Button button_photo1;
    private Button button_photo2;
    private Button button_tt_send;

    String selectionTextViewA = "";

    private String actyalRec_id = "";
    private File photo_path_single;

    private String photo1_path_single = "";
    ArrayList<Uri> photo1_path_multiple = new ArrayList<Uri>();

    private String photo3_path_single = "";
    ArrayList<Uri> photo3_path_multiple = new ArrayList<Uri>();


    boolean init = false;
    boolean fromMap = false;

    Geocoder geocoder;

    public void GetLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_READ_LOCATION);
        }
    }

    LocationManager locationManager;
    Context mContext;

    public void onLocationChanged(Location location) {
        try {
            geocoder = new Geocoder(this, Locale.getDefault());
            if (location != null) {
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 10);
            }

        } catch (IOException e) {
            Log.e("LocateMe", "Could not get Geocoder data", e);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anketa_full);

        Button button_foto1 = (Button) this.findViewById(R.id.button_photo1);
        Button button_foto2 = (Button) this.findViewById(R.id.button_photo2);
        Button button_tt_send = (Button) this.findViewById(R.id.button_tt_send);

        editTextName = (EditText) this.findViewById(R.id.editTextName);
        editTextTAddress = (EditText) this.findViewById(R.id.editTextTAddress);
        editTextTLatitude = (EditText) this.findViewById(R.id.editTextTLatitude);
        editTextTLongitude = (EditText) this.findViewById(R.id.editTextTLongitude);
        editTextTS1 = (EditText) this.findViewById(R.id.editTextTS1);
        editTextTS2 = (EditText) this.findViewById(R.id.editTextTS2);
        spinnerKanal = (Spinner)findViewById(R.id.spinnerKanal);
        spinnerSostav = (Spinner)findViewById(R.id.spinnerSostav);
        editTextContact = (EditText) this.findViewById(R.id.editTextContact);
        editTextTPСontactNumber = (EditText) this.findViewById(R.id.editTextTPСontactNumber);
        spinnerPost = (Spinner)findViewById(R.id.spinnerPost);
        editTextGk = (EditText) this.findViewById(R.id.editTextGk);
        editTextUl = (EditText) this.findViewById(R.id.editTextUl);
        editTextInn = (EditText) this.findViewById(R.id.editTextInn);
        editTextTAddressD = (EditText) this.findViewById(R.id.editTextTAddressD);
        editTextTPPA = (EditText) this.findViewById(R.id.editTextTPPA);
        spinnerFT = (Spinner)findViewById(R.id.spinnerFT);
        spinnerTipTT = (Spinner)findViewById(R.id.spinnerTipTT);
        spinnerKk = (Spinner)findViewById(R.id.spinnerKk);


        // Список выпадающий (Дон_)
        ArrayList<String> ar = new ArrayList<String>();
        for (int i = 0; i < 100; i++) {
            ar.add("Дон_" + i);
        }
        AutoCompleteTextView textViewA = (AutoCompleteTextView)
                findViewById(R.id.AutoCompleteTextViewTorgTer);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, ar);
        textViewA.setAdapter(adapter);

        textViewA.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                selectionTextViewA = (String)parent.getItemAtPosition(position);
            }
        });

        String AutoCompleteTextView = textViewA.getEditableText().toString();

        // Клик по "Соответствует адресу ТТ"
        CheckBox checkBoxAddressD = (CheckBox) findViewById(R.id.checkBoxAddressD);
        EditText editTextTAddressD = findViewById(R.id.editTextTAddressD);

        checkBoxAddressD.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    if(!editTextTAddress.getText().toString().equals("")) {
                        editTextTAddressD.setText(editTextTAddress.getText());
                        editTextTAddressD.setFocusableInTouchMode(true);
                        editTextTAddressD.setEnabled(false);
                    }else{
                        checkBoxAddressD.setChecked (false);
                        Toast.makeText(getApplicationContext(), "Необходимо заполнить адрес магазина", Toast.LENGTH_SHORT).show();
                    }else{
                    editTextTAddressD.setEnabled(true);
                }
            }
        });

        spinnerKanal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String sk = String.valueOf(spinnerKanal.getSelectedItem());
                setValueSpinner(sk, "", -1);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        this.imageViewGetLocate = (ImageView) findViewById(R.id.imageViewGetLocate);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mContext = this;
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);
        Cursor query;

        try {
            String frame_top_address = getIntent().getStringExtra("frame_top_address");
            String frame_top_latitude = getIntent().getStringExtra("frame_top_latitude");
            String frame_top_longitude = getIntent().getStringExtra("frame_top_longitude");
            rec_id = getIntent().getStringExtra("rec_id");
            status = getIntent().getIntExtra("status", 0);
            fromMap = getIntent().getBooleanExtra("map", false);

            editTextTAddress.setText(frame_top_address);
            editTextTLatitude.setText(frame_top_latitude);
            editTextTLongitude.setText(frame_top_longitude);

            // удаляем незаконченные
            db.execSQL("DELETE FROM questionnaires WHERE finished == 0 or  name == null;");

            String name, kateg_f, name_net, torgter, tip, s_i, s_n, formtorg, tel, contact, post, kanal, sostav;

            // Красная ТТ из БД
            if (status == 1) {
                final Random random = new Random();
                actyalRec_id = "pt_" + random.nextInt(1000000);
                db.execSQL("INSERT INTO questionnaires (rec_id) VALUES ('" + actyalRec_id + "');");
                setValueAnketa();
            // ПТ из БД (ОРАНЖЕВАЯ)
            } else if (status == 2) {
                Log.v("ПТ из БД (ОРАНЖЕВАЯ)", rec_id);
                setValueAnketa();
                // Потенциальная ТТ с сервера(ОРАНЖ)
            } else if (status == 3) {
                Log.v("Потенциальная ТТ с сервера(ОРАНЖ не ред)", rec_id);

                editTextTAddress.setEnabled(false);
                editTextName.setEnabled(false);
                editTextTS1.setEnabled(false);
                editTextTS2.setEnabled(false);
                spinnerKanal.setEnabled(false);
                editTextContact.setEnabled(false);
                editTextTPСontactNumber.setEnabled(false);
                spinnerPost.setEnabled(false);
                spinnerSostav.setEnabled(false);
                spinnerSostav.setEnabled(false);
                spinnerKk.setEnabled(false);
                spinnerTipTT.setEnabled(false);
                spinnerFT.setEnabled(false);
                editTextGk.setEnabled(false);
                editTextUl.setEnabled(false);
                editTextInn.setEnabled(false);
                editTextTPPA.setEnabled(false);
                textViewA.setEnabled(false);
                editTextTAddressD.setEnabled(false);
                checkBoxAddressD.setVisibility(View.GONE);

                imageViewGetLocate.setVisibility(View.GONE);
                button_foto1.setVisibility(View.GONE);
                button_foto2.setVisibility(View.GONE);
                button_tt_send.setVisibility(View.GONE);
                setValueAnketa();

            } else if (status == 0) {
                final Random random = new Random();
                actyalRec_id = "pt_" + random.nextInt(1000000);
                db.execSQL("INSERT INTO questionnaires (rec_id) VALUES ('" + actyalRec_id + "');");
            }
        } catch (Exception e) {
        }

        button_foto1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 102);
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                } else {
                    name_table_photo = "PhotoReport";
                    selectImage();
                }
            }
        });

        button_foto2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 102);
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                } else {
                    name_table_photo = "PhotoStore";
                    selectImage();
                }
            }
        });

        // Метод обработки нажатия на отправить
        boolean finalFromMap = fromMap;
        button_tt_send.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString().trim();
                String address = editTextTAddress.getText().toString().trim();
                String latitude = editTextTLatitude.getText().toString().trim();
                String longitude = editTextTLongitude.getText().toString().trim();
                String s_i = editTextTS1.getText().toString().trim();
                String s_n = editTextTS2.getText().toString().trim();
                String kanal = spinnerKanal.getSelectedItem().toString();
                String sostav = spinnerSostav.getSelectedItem().toString();
                String contact = editTextContact.getText().toString().trim();
                String tel = editTextTPСontactNumber.getText().toString().trim();
                String name_net = editTextGk.getText().toString().trim();
                String ul = editTextUl.getText().toString().trim();
                String inn = editTextInn.getText().toString().trim();
                String addressd = editTextTAddressD.getText().toString().trim();
                String post_pa = editTextTPPA.getText().toString().trim();
                String torgter = selectionTextViewA;

                String kateg_f = spinnerKk.getSelectedItem().toString();
                String tip = spinnerTipTT.getSelectedItem().toString();

                String post = spinnerPost.getSelectedItem().toString();
                String formtorg = spinnerFT.getSelectedItem().toString();


                SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);

                if (name.trim().equals("")) {
                    editTextName.setError("Необходимо заполнить");
                    return;
                }
                if (address.trim().equals("")) {
                    editTextTAddress.setError("Необходимо заполнить");
                    return;
                }
                if (latitude.trim().equals("")) {
                    editTextTLatitude.setError("Необходимо заполнить");
                    return;
                }
                if (longitude.trim().equals("")) {
                    editTextTLongitude.setError("Необходимо заполнить");
                    return;
                }

                //точка из БД (красная)
                if (status == 1) {

                    Log.v("ТОЧКА КРАСНАЯ", "-----------------");
                    final Random random = new Random();
                    actyalRec_id = "pt_" + random.nextInt(1000000);
                    db.execSQL("INSERT INTO questionnaires (rec_id) VALUES ('" + actyalRec_id + "');");

                    //удаляем красную точку, делаем желтой потом
                    db.execSQL("DELETE FROM tt_info WHERE rec_id == '" + actyalRec_id + "';");
                } else {
                }

                Log.v("UPDATE_questionnaires_actyalRec_id", actyalRec_id);

                latitude = latitude.replace(",", ".");
                longitude = longitude.replace(",", ".");
                name = name.replace("'", "").replace("\"", "");
                address = address.replace("'", "").replace("\"", "");
                db.execSQL("UPDATE questionnaires SET " +
                        "name = '" + name + "'," +
                        "address = '" + address + "'," +
                        "latitude = '" + latitude + "'," +
                        "longitude = '" + longitude + "'," +
                        "kateg_f = '" + kateg_f + "'," +
                        "name_net = '" + name_net + "'," +
                        "ul = '" + ul + "'," +
                        "inn = '" + inn + "'," +
                        "torgter = '" + torgter + "'," +
                        "addressd = '" + addressd + "'," +
                        "tip = '" + tip + "'," +
                        "s_i = '" + s_i + "'," +
                        "s_n = '" + s_n + "'," +
                        "formtorg = '" + formtorg + "'," +
                        "tel = '" + tel + "'," +
                        "contact = '" + contact + "'," +
                        "post = '" + post + "'," +
                        "kanal = '" + kanal + "'," +
                        "sostav = '" + sostav + "'," +
                        "post_pa = '" + post_pa + "'," +
                        "sync = '0'," +
                        "finished = 1," +
                        "status = 2," +
                        "date_created = datetime() " +
                        "WHERE rec_id = '" + actyalRec_id + "'");


                // сохраняем пути к картинкам в БД
                saveImageInDB(photo1_path_multiple, photo1_path_single, "PhotoReport");
                saveImageInDB(photo3_path_multiple, photo3_path_single, "PhotoStore");

                Toast.makeText(getApplicationContext(), "Данные успешно добавлены", LENGTH_LONG).show();


                // если анкета открыта из карты
                if (finalFromMap) {

                    //пересоздаем maps
                    Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                    intent.putExtra("latitude", latitude);
                    intent.putExtra("longitude", longitude);
                    intent.putExtra("fromanketa", 1);
                    startActivity(intent);
                    finish();
                } else {
                    Intent refresh = new Intent(getApplicationContext(), AnketaTTFullActivity.class);
                    startActivity(refresh);
                    finish();
                }
            }

            private void saveImageInDB(ArrayList<Uri> photo_path_multiple, String photo_path_single, String db_name) {
                SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);
                if ((photo_path_multiple.size() > 0) || !photo_path_single.equals("")) {
                    for (Iterator<Uri> i = photo_path_multiple.iterator(); i.hasNext(); ) {
                        Uri item = i.next();
                        Log.v("photo_path_multiple", String.valueOf(db_name));
                        Log.v("photo_path_multiple", String.valueOf(actyalRec_id));
                        Log.v("photo_path_multiple", String.valueOf(item));
                        db.execSQL("INSERT INTO '" + db_name + "' (rec_id, path, sync) VALUES ('" + actyalRec_id + "', '" + item + "', '0' )");
                    }
                    if (!photo_path_single.equals("")) {
                        Log.v("photo_path_single", String.valueOf(db_name));
                        Log.v("photo_path_single", String.valueOf(actyalRec_id));
                        Log.v("photo_path_single", String.valueOf(photo_path_single));
                        db.execSQL("INSERT INTO  '" + db_name + "'  (rec_id, path, sync) VALUES ('" + actyalRec_id + "', '" + photo_path_single + "', '0')");
                    }
                }
            }
        });

        imageViewGetLocate.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Поиск местоположения...", Toast.LENGTH_SHORT).show();

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        3000, 3, locationListenerGPS);
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 3000, 3,
                        locationListenerGPS);
            }
        });
    }

    private void setValueAnketa(){
        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);
        Cursor query;
        String name, kateg_f, name_net, ul, inn, torgter, addressd, tip, s_i, s_n, formtorg, tel, contact, post, kanal, sostav, post_pa;

        // Если ТТ красная
        if (status == 1){
            query = db.rawQuery("SELECT name, kateg_f, name_net, torgter, tip," +
                    "s_i, s_n, formtorg, tel, contact, post, kanal, sostav" +
                    " FROM tt_info WHERE rec_id = '" + rec_id + "';", null);
        }else {
            query = db.rawQuery("SELECT name, kateg_f, name_net, torgter, tip, s_i, s_n, " +
                    "formtorg, tel, contact, post, kanal, sostav, " +
                    "ul, inn, addressd,  post_pa" +
                    " FROM questionnaires WHERE rec_id = '" + rec_id + "';", null);
        }

        if (query != null) {
            if (query.moveToFirst()) {
                String[] baths;
                name = query.getString(0);
                kateg_f = query.getString(1);
                name_net = query.getString(2);
                torgter = query.getString(3);
                tip = query.getString(4);
                s_i = query.getString(5);
                s_n = query.getString(6);
                formtorg = query.getString(7);
                tel = query.getString(8);
                contact = query.getString(9);
                post = query.getString(10);
                kanal = query.getString(11);
                sostav = query.getString(12);
                if (status != 1) {
                    ul = query.getString(13);
                    inn = query.getString(14);
                    addressd = query.getString(15);
                    post_pa = query.getString(16);

                    editTextUl.setText(ul);
                    editTextInn.setText(inn);
                    editTextTAddressD.setText(addressd);
                    editTextTPPA.setText(post_pa);
                }

                AutoCompleteTextView textViewA = (AutoCompleteTextView)
                        findViewById(R.id.AutoCompleteTextViewTorgTer);
                textViewA.setText(torgter);
                editTextName.setText(name);
                editTextTS1.setText(s_i);
                editTextTS2.setText(s_n);
                editTextGk.setText(name_net);
                editTextContact.setText(contact);
                editTextTPСontactNumber.setText(tel);

                baths = getResources().getStringArray(R.array.anketa_kanal);
                spinnerKanal.setSelection(Arrays.asList(baths).indexOf("" + kanal + ""));

                baths = getResources().getStringArray(R.array.anketa_post);
                spinnerPost.setSelection(Arrays.asList(baths).indexOf("" + post + ""));

                baths = getResources().getStringArray(R.array.anketa_kk);
                spinnerKk.setSelection(Arrays.asList(baths).indexOf("" + kateg_f + ""));

                baths = getResources().getStringArray(R.array.anketa_tiptt);
                spinnerTipTT.setSelection(Arrays.asList(baths).indexOf("" + tip + ""));

                baths = getResources().getStringArray(R.array.anketa_ft);
                spinnerFT.setSelection(Arrays.asList(baths).indexOf("" + formtorg + ""));


                int value = 0;
                if (kanal.contentEquals("Бизнес для бизнеса")) value = R.array.anketa_sostav1;
                if (kanal.contentEquals("Магазины")) value = R.array.anketa_sostav2;
                if (kanal.contentEquals("ОПТ")) value = R.array.anketa_sostav3;
                if (kanal.contentEquals("Рынки")) value = R.array.anketa_sostav4;
                if (kanal.contentEquals("Сети")) value = R.array.anketa_sostav5;

                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                        value,
                        android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerSostav.setAdapter(adapter);
                spinnerSostav.setVisibility(View.VISIBLE);

                String[] baths2 = getResources().getStringArray(value);
                spinnerSostav.setSelection(Arrays.asList(baths2).indexOf("" + sostav + ""));

                baths = getResources().getStringArray(R.array.anketa_kk);
                spinnerKk.setSelection(Arrays.asList(baths).indexOf("" + kateg_f + ""));

                baths = getResources().getStringArray(R.array.anketa_tiptt);
                spinnerTipTT.setSelection(Arrays.asList(baths).indexOf("" + tip + ""));

                baths = getResources().getStringArray(R.array.anketa_ft);
                spinnerFT.setSelection(Arrays.asList(baths).indexOf("" + formtorg + ""));
                actyalRec_id = rec_id;
            }
        }
    }

    public void setValueSpinner(String sk, String set, int status) {
        int value = 0;
        if(sk.contentEquals("Бизнес для бизнеса")) value = R.array.anketa_sostav1;
        if(sk.contentEquals("Магазины")) value = R.array.anketa_sostav2;
        if(sk.contentEquals("ОПТ")) value = R.array.anketa_sostav3;
        if(sk.contentEquals("Рынки")) value = R.array.anketa_sostav4;
        if(sk.contentEquals("Сети")) value = R.array.anketa_sostav5;

        // замена при выборе
        if (value != 0 && status == -1 && init)  {
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                    value,
                    android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSostav.setAdapter(adapter);
            spinnerSostav.setVisibility(View.VISIBLE);
        }
        init = true;
    }


    @Override
    public void onBackPressed() {
        if (fromMap) {
            //пересоздаем maps
            Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
            startActivity(intent);
            finish();
        } else{
            finish();
        }
    }

    LocationListener locationListenerGPS=new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            editTextTLatitude.setText(String.format("%.6f", latitude));
            editTextTLongitude.setText(String.format("%.6f", longitude));

            try {
                List<Address> addresses = new ArrayList<>();
                Geocoder myGeo = new Geocoder(getApplicationContext(), Locale.getDefault());
                if (location != null) {
                    addresses = myGeo.getFromLocation(location.getLatitude(), location.getLongitude(), 10);
                }
                try {
                    Address address = addresses.get(0);

                    if (address != null) {
                        String addr = address.getAddressLine(0);
                        editTextTAddress.setText(addr);
                    }
                } catch (Exception e) {
                    editTextTAddress.setText("Нет информации");
                }

            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Ошибка получяения адреса местоположения", Toast.LENGTH_SHORT).show();
            }
            locationManager.removeUpdates(this);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK
                    && null != data) {
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                imagesEncodedList = new ArrayList<String>();
                if(data.getData()!=null){
                    String path = ImageFilePath.getPath(this, data.getData());

                    if (name_table_photo.equals("PhotoReport")){
                        if(!photo1_path_multiple.contains(Uri.parse(path)))
                            photo1_path_multiple.add(Uri.parse(path));
                        Log.v("data.getData()1", String.valueOf(Uri.parse(path)));
                    } else if (name_table_photo.equals("PhotoStore")) {
                        if(!photo3_path_multiple.contains(Uri.parse(path)))
                            photo3_path_multiple.add(Uri.parse(path));
                        Log.v("data.getData()3", String.valueOf(Uri.parse(path)));
                    }
                    Uri mImageUri=data.getData();
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imageEncoded  = cursor.getString(columnIndex);
                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();
                        ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            mArrayUri.add(uri);
                            String path = ImageFilePath.getPath(this, uri);
                            if (name_table_photo.equals("PhotoReport")) {
                                if (!photo1_path_multiple.contains(Uri.parse(path))) {
                                    photo1_path_multiple.add(Uri.parse(path));
                                    for (Iterator<Uri> i2 = photo1_path_multiple.iterator(); i2.hasNext(); ) {
                                        Uri item2 = i2.next();
                                        fastCompressBit(String.valueOf(item2));
                                    }
                                }
                            } else if (name_table_photo.equals("PhotoStore")) {
                                if (!photo3_path_multiple.contains(Uri.parse(path))) {
                                    photo3_path_multiple.add(Uri.parse(path));
                                    for (Iterator<Uri> i2 = photo3_path_multiple.iterator(); i2.hasNext(); ) {
                                        Uri item2 = i2.next();
                                        fastCompressBit(String.valueOf(item2));
                                    }
                                }
                            }
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            cursor.moveToFirst();
                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            imageEncoded  = cursor.getString(columnIndex);
                            imagesEncodedList.add(imageEncoded);
                        }
                    }
                }
                Toast.makeText(getApplicationContext(), "Изображение прикреплено", Toast.LENGTH_SHORT).show();


            } else if  (requestCode == 2 && resultCode == -1) {
                fastCompressBit(String.valueOf(photo_path_single));

                if (name_table_photo.equals("PhotoReport"))
                    photo1_path_multiple.add(Uri.parse(String.valueOf(photo_path_single)));
                else
                    photo3_path_multiple.add(Uri.parse(String.valueOf(photo_path_single)));
                Toast.makeText(getApplicationContext(), "Изображение прикреплено", Toast.LENGTH_SHORT).show();
            }
            else {
                photo_path_single.delete();
                Toast.makeText(this, "Изображение не прикреплено",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Изображение не прикреплено", Toast.LENGTH_LONG)
                    .show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void fastCompressBit(String path) {
        // быстрое сжатие
        int quality = 20;
        Bitmap bMap = BitmapFactory.decodeFile(path);

        try (FileOutputStream out = new FileOutputStream(path)) {
            bMap.compress(Bitmap.CompressFormat.JPEG, quality, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void selectImage() {
        final CharSequence[] options = { "Фото", "Открыть галерею"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Добавить фото");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Фото"))
                {
                    try {
                        photo_path_single = openCamera();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                else if (options[item].equals("Открыть галерею"))
                {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,""), PICK_IMAGE_MULTIPLE);
                }
            }

        });

        builder.show();

    }

    private File openCamera() throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile(this);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI =  FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 2);
            }
            return photoFile;
        }
        return null;
    }

    private File createImageFile(AnketaTTFullActivity anketaTTActivity) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = timeStamp;
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        return image;
    }
}
