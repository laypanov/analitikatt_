package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class SyncActivity extends AppCompatActivity {

    private TextView textView_sync_logs;
    private TextView textView_sync_markers_objects;
    private TextView textView_sync_unsync_objects;
    private TextView textView_sync_unsync_objects2;
    private Button sync_action_btn;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);
        SyncObjectsTextView();
    }

    public void SyncObjectsTextView() {
        try {
            textView_sync_markers_objects = (TextView) this.findViewById(R.id.textView_sync_markers_objects);
            textView_sync_unsync_objects = (TextView) this.findViewById(R.id.textView_sync_unsync_objects);
            textView_sync_unsync_objects2 = (TextView) this.findViewById(R.id.textView_sync_unsync_objects2);

            SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);
            String countQuery;
            Cursor cursor;
            String unsync_photo;
            int count;

            countQuery = "SELECT  rec_id FROM tt_info";
            cursor = db.rawQuery(countQuery, null);
            count = cursor.getCount();
            textView_sync_markers_objects.setText(String.valueOf(count));
            cursor.close();

            countQuery = "SELECT rec_id FROM questionnaires WHERE sync = 1";
            cursor = db.rawQuery(countQuery, null);
            String sync_questionnaires = textView_sync_markers_objects.getText().toString();
            textView_sync_markers_objects.setText(sync_questionnaires + " \\ " + cursor.getCount());
            cursor.close();

            countQuery = "SELECT rec_id FROM questionnaires WHERE sync = '0'";
            cursor = db.rawQuery(countQuery, null);
            count = cursor.getCount();
            textView_sync_unsync_objects.setText(String.valueOf(count));
            cursor.close();

            countQuery = "SELECT rec_id FROM PhotoReport WHERE sync = '0'";
            cursor = db.rawQuery(countQuery, null);
            count = cursor.getCount();
            textView_sync_unsync_objects2.setText(String.valueOf(count));
            cursor.close();

            countQuery = "SELECT rec_id FROM PhotoStore WHERE sync = '0'";
            cursor = db.rawQuery(countQuery, null);
            unsync_photo = textView_sync_unsync_objects2.getText().toString();
            textView_sync_unsync_objects2.setText(unsync_photo + " \\ " + cursor.getCount());
            cursor.close();

            db.close();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Необходимо выполнить синхронизацию", Toast.LENGTH_LONG).show();
            return;
        }
    }

    public void SetTextLog(String new_data_sync) {
        textView_sync_logs = (TextView) this.findViewById(R.id.textView_sync_logs);

        String sync_logs;
        sync_logs = textView_sync_logs.getText().toString();
        textView_sync_logs.setText(sync_logs + "\n" + new_data_sync);
    }

    public void onClick_sync_action_btn2(View view) throws Exception {

        textView_sync_logs = (TextView) this.findViewById(R.id.textView_sync_logs);
        textView_sync_logs.setText("");
        String uuid = "";
        String address_server = "";

        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);
        Cursor query = db.rawQuery("SELECT uuid, address_server  FROM auth_user;", null);
        if (query.moveToFirst()) {
            uuid = query.getString(0);
            address_server = query.getString(1);
        }
        query.close();
        db.close();

        createDB();
        SetTextLog("\n Выгрузка анкет ТТ");
        uploadToServer(uuid, address_server);

        SetTextLog("\n Выгрузка фото фасад");
        uploadImageToServer(uuid, address_server, "PhotoReport");

        SetTextLog("\n Выгрузка фото подробный");
        uploadImageToServer(uuid, address_server, "PhotoStore");

        SetTextLog("\n Загрузка актуальных ТТ");
        downloadFromServer(uuid, address_server);
        SyncObjectsTextView();
    }


    public boolean createDB() {
        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);
        try {
            SetTextLog("Проверяем состояние БД");

            db.execSQL("CREATE TABLE IF NOT EXISTS tt_info (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "rec_id INT  UNIQUE, " +
                    "name TEXT, " +
                    "address TEXT, " +
                    "town TEXT," +
                    "region TEXT," +
                    "latitude REAL," +
                    "longitude REAL," +
                    "kateg_f TEXT," +
                    "name_net TEXT," +
                    "torgter TEXT," +
                    "tip TEXT," +
                    "s_i TEXT," +
                    "s_n TEXT," +
                    "formtorg TEXT," +
                    "tel TEXT," +
                    "contact TEXT," +
                    "post TEXT," +
                    "kanal TEXT," +
                    "sostav TEXT)");

            // db.execSQL("DROP TABLE IF EXISTS options;");
            db.execSQL("CREATE TABLE IF NOT EXISTS options (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, " +
                    "map_distance_markers INT)");

            // db.execSQL("DROP TABLE IF EXISTS gps_track;");
            db.execSQL("CREATE TABLE IF NOT EXISTS gps_track (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, " +
                    "latitude DOUBLE," +
                    "longitude DOUBLE," +
                    "date_created DATE)");

            // db.execSQL("DROP TABLE IF EXISTS questionnaires;");
            db.execSQL("CREATE TABLE IF NOT EXISTS questionnaires (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "rec_id TEXT UNIQUE, " +
                    "name TEXT, " +
                    "address TEXT, " +
                    "latitude DOUBLE," +
                    "longitude DOUBLE," +
                    "kateg_f TEXT," +
                    "name_net TEXT," +
                    "ul TEXT," +
                    "inn TEXT," +
                    "torgter TEXT," +
                    "addressd TEXT," +
                    "tip TEXT," +
                    "s_i TEXT," +
                    "s_n TEXT," +
                    "formtorg TEXT," +
                    "tel TEXT," +
                    "contact TEXT," +
                    "post TEXT," +
                    "kanal TEXT," +
                    "sostav TEXT," +
                    "post_pa TEXT," +
                    "PhotoReport INT," +
                    "PhotoStore INT," +
                    "sync INT," +
                    "finished INT," +
                    "status INT," +
                    "date_created DATE)");

            // удаляем незаконченные
            db.execSQL("DELETE FROM questionnaires WHERE finished != 1");

            //db.execSQL("DROP TABLE IF EXISTS PhotoReport;");
            db.execSQL("CREATE TABLE IF NOT EXISTS PhotoReport (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "rec_id TEXT, " +
                    "path TEXT, " +
                    "sync INT)");

            //db.execSQL("DROP TABLE IF EXISTS PhotoStore;");
            db.execSQL("CREATE TABLE IF NOT EXISTS PhotoStore (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "rec_id TEXT, " +
                    "path TEXT, " +
                    "sync INT)");

            // 500 метром до ТТ
            db.execSQL("INSERT OR IGNORE INTO options (_id, map_distance_markers) " +
                    "VALUES (1, 1000)");
            db.close();

        } catch (SQLException e) {
            SetTextLog("------ ОШИБКА");
            e.printStackTrace();
        }
        db.close();
        SetTextLog("------ успешно");
        return true;
    }

    public boolean uploadToServer(String uuid, String address_server) {
        try {
            JSONArray jsonParam_common = new JSONArray();
            JSONArray jsonParam2 = new JSONArray();
            SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);

            Cursor query = db.rawQuery("SELECT rec_id, name, address, latitude, longitude, " +
                    "kateg_f, name_net, ul, inn, torgter, addressd, tip, s_i, s_n, formtorg, tel, " +
                    "contact, post, kanal, sostav, post_pa,  date_created " +
                    "FROM questionnaires WHERE sync == '0'", null);
            if (query != null) {
                if (query.moveToFirst()) {
                    do {
                        String rec_id = query.getString(0);
                        String name = query.getString(1);
                        String address = query.getString(2);
                        Double latitude = query.getDouble(3);
                        Double longitude = query.getDouble(4);
                        String kateg_f = query.getString(5);
                        String name_net = query.getString(6);
                        String ul = query.getString(7);
                        String inn = query.getString(8);
                        String torgter = query.getString(9);
                        String addressd = query.getString(10);
                        String tip = query.getString(11);
                        String s_i = query.getString(12);
                        String s_n = query.getString(13);
                        String formtorg = query.getString(14);
                        String tel = query.getString(15);
                        String contact = query.getString(16);
                        String post = query.getString(17);
                        String kanal = query.getString(18);
                        String sostav = query.getString(19);
                        String post_pa = query.getString(20);
                        String date_created = query.getString(21);

                        JSONObject jsonObject_buffer = new JSONObject();
                        jsonObject_buffer.put("rec_id", rec_id);
                        jsonObject_buffer.put("name", name);
                        jsonObject_buffer.put("address", address);
                        jsonObject_buffer.put("latitude", latitude);
                        jsonObject_buffer.put("longitude", longitude);
                        jsonObject_buffer.put("kateg_f", kateg_f);
                        jsonObject_buffer.put("name_net", name_net);
                        jsonObject_buffer.put("ul", ul);
                        jsonObject_buffer.put("inn", inn);
                        jsonObject_buffer.put("torgter", torgter);
                        jsonObject_buffer.put("addressd", addressd);
                        jsonObject_buffer.put("tip", tip);
                        jsonObject_buffer.put("s_i", s_i);
                        jsonObject_buffer.put("s_n", s_n);
                        jsonObject_buffer.put("formtorg", formtorg);
                        jsonObject_buffer.put("tel", tel);
                        jsonObject_buffer.put("contact", contact);
                        jsonObject_buffer.put("post", post);
                        jsonObject_buffer.put("kanal", kanal);
                        jsonObject_buffer.put("sostav", sostav);
                        jsonObject_buffer.put("post_pa", post_pa);
                        jsonObject_buffer.put("date_created", date_created);
                        jsonParam2.put(jsonObject_buffer);

                        SetTextLog("------ анкета [ " + rec_id + " ]");
                    } while (query.moveToNext());

                    JSONObject jsonObject_common_buffer = new JSONObject();
                    jsonObject_common_buffer.put("uuid", uuid);
                    jsonObject_common_buffer.put("process", "JSON");
                    jsonObject_common_buffer.put("post_url", "http://" + address_server + "/sync/upload/json/?set_questionnaires=1");
                    jsonParam_common.put(jsonObject_common_buffer);

                    JSONObject rootObject = new JSONObject();
                    rootObject.put("common", jsonParam_common);
                    rootObject.put("questionnaires", jsonParam2);

                    String json_data = new PostImageToServerAsync().execute(rootObject).get();
                    JSONObject jObject = new JSONObject(json_data);
                    if (jObject.getString("upload").equals("done")) {
                        db.execSQL("UPDATE questionnaires SET sync = '1', status = '3'");
                        return true;
                    } else {
                        SetTextLog("------ ошибка при выгрузке");
                    }
                }
                query.close();
                SetTextLog("------ нет данных для оптравки");
            }
        } catch (InterruptedException interruptedException) {
            SetTextLog("------ ошибка при выгрузке");
            interruptedException.printStackTrace();
        } catch (ExecutionException executionException) {
            SetTextLog("------ ошибка при выгрузке");
            executionException.printStackTrace();
        } catch (JSONException jsonException) {
            SetTextLog("------ ошибка при выгрузке");
            jsonException.printStackTrace();
        }
        return true;
    }

    public boolean uploadImageToServer(String uuid, String address_server, String db_name) {
        try {
            SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);
            Cursor query2 = db.rawQuery("SELECT rec_id, path FROM  '" +  db_name + "' WHERE sync == '0'", null);
            if (query2 != null) {
                if (query2.moveToFirst()) {
                    while (!query2.isAfterLast()) {
                        JSONArray jsonParam_common2 = new JSONArray();
                        JSONArray jsonParam3 = new JSONArray();
                        JSONObject jsonObject_common_buffer2 = new JSONObject();
                        JSONObject jsonObject_buffer2 = new JSONObject();
                        JSONObject rootObject2 = new JSONObject();

                        jsonObject_common_buffer2.put("uuid", uuid);
                        jsonObject_common_buffer2.put("process", "IMAGE");
                        jsonObject_common_buffer2.put("post_url", "http://" + address_server + "/sync/upload/json/?set_photos=1");

                        rootObject2.put("common", jsonParam_common2);
                        jsonParam_common2.put(jsonObject_common_buffer2);

                        String rec_id2 = query2.getString(0);
                        String path = query2.getString(1);

                        jsonObject_buffer2.put("rec_id", rec_id2);
                        jsonObject_buffer2.put("path", path);
                        jsonObject_buffer2.put("db_name", db_name);
                        jsonParam3.put(jsonObject_buffer2);

                        rootObject2.put("set_photos", jsonParam3);

                        String json_data = new PostImageToServerAsync().execute(rootObject2).get();

                        if (json_data.equals("False")) {
                            SetTextLog("----- отсутствует соединение с сервером");
                            Toast.makeText(getApplicationContext(), "Отсутствует соединение с сервером", Toast.LENGTH_LONG).show();
                            return false;
                        }

                        JSONObject jObject = new JSONObject(json_data);
                        if (jObject.getString("upload").equals("done")) {
                            SetTextLog("------ отправка фото [ " + rec_id2 + " ]");
                            db.execSQL("UPDATE '" +  db_name + "' SET sync = '1' WHERE rec_id == '" +  rec_id2 + "' ");
                        }else{
                            SetTextLog("------ ошибка при выгрузке [ " + rec_id2 + " ]");
                        }
                        query2.moveToNext();
                    }
                    SetTextLog("------ успешно");
                }
                SetTextLog("------ нет данных для отправки");

            }
        } catch (InterruptedException interruptedException) {
            SetTextLog("------ ошибка при выгрузке");
            interruptedException.printStackTrace();
        } catch (ExecutionException executionException) {
            SetTextLog("------ ошибка при выгрузке");
            executionException.printStackTrace();
        } catch (JSONException jsonException) {
            SetTextLog("------ ошибка при выгрузке");
            jsonException.printStackTrace();
        }
        return true;
    }

    public void downloadFromServer(String uuid, String address_server) {
        try {
            SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);
            JSONArray jsonParam_common = new JSONArray();
            JSONObject jsonObject_common_buffer = new JSONObject();
            JSONObject rootObject = new JSONObject();
            JSONObject oneObject = new JSONObject();

            String rec_id = "";
            String name = "";
            String address = "";
            String latitude = "";
            String longitude = "";
            String square = "";
            String kanal = "";
            String s_mehi = "";
            String s_mshi = "";
            String s_ss = "";
            String s_hoz = "";
            String s_so = "";
            String sostav = "";
            String s_i = "";
            String town = "";
            String kateg_f = "";
            String name_net = "";
            String torgter = "";
            String tip = "";
            String s_n = "";
            String formtorg = "";
            String tel = "";
            String contact = "";
            String post = "";
            String region = "";
            String ul = "";
            String inn = "";
            String addressd = "";
            String sync = "";
            int i = 0;

            jsonObject_common_buffer.put("uuid", uuid);
            jsonObject_common_buffer.put("process", "JSON");
            jsonObject_common_buffer.put("post_url", "http://" + address_server + "/sync/download/json/?get_tt=1");
            jsonParam_common.put(jsonObject_common_buffer);
            rootObject.put("common", jsonParam_common);

            String json_data = new PostImageToServerAsync().execute(rootObject).get();
            if (json_data.equals("False")){
                SetTextLog("----- отсутствует соединение с сервером");
                Toast.makeText(getApplicationContext(), "Отсутствует соединение с сервером", Toast.LENGTH_LONG).show();
                return;
            }
            JSONObject jObject = new JSONObject(json_data);
            JSONArray jArray_tt_info = jObject.getJSONArray("tt_info");

            if (jArray_tt_info.length() == 0) {
                SetTextLog("\n------ не получена информация о ТТ");
            } else {
                SetTextLog("------ информация получена, обновляем");
            }

            String sql = "INSERT OR REPLACE INTO tt_info (" +
                    "rec_id, name, address, latitude, longitude, kateg_f, name_net, torgter, " +
                    "tip, s_i, s_n, formtorg, tel, contact, post, kanal, sostav, town, region) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            db.beginTransaction();
            SQLiteStatement statement = db.compileStatement(sql);

            for (i = 0; i < jArray_tt_info.length(); i++) {
                oneObject = jArray_tt_info.getJSONObject(i);

                rec_id = oneObject.getString("rec_id");
                name = oneObject.getString("name");
                address = oneObject.getString("address");
                latitude = oneObject.getString("latitude");
                longitude = oneObject.getString("longitude");
                kateg_f = oneObject.getString("kateg_f");
                name_net = oneObject.getString("name_net");
                torgter = oneObject.getString("torgter");
                tip = oneObject.getString("tip");
                s_i = oneObject.getString("s_i");
                s_n = oneObject.getString("s_n");
                formtorg = oneObject.getString("formtorg");
                tel = oneObject.getString("tel");
                contact = oneObject.getString("contact");
                post = oneObject.getString("post");
                kanal = oneObject.getString("kanal");
                sostav = oneObject.getString("sostav");
                town = oneObject.getString("town");
                region = oneObject.getString("region");

                statement.bindString(1, rec_id);
                statement.bindString(2, name);
                statement.bindString(3, address);
                statement.bindString(4, latitude);
                statement.bindString(5, longitude);
                statement.bindString(6, kateg_f);
                statement.bindString(7, name_net);
                statement.bindString(8, torgter);
                statement.bindString(9, tip);
                statement.bindString(10, s_i);
                statement.bindString(11, s_n);
                statement.bindString(12, formtorg);
                statement.bindString(13, tel);
                statement.bindString(14, contact);
                statement.bindString(15, post);
                statement.bindString(16, kanal);
                statement.bindString(17, sostav);
                statement.bindString(18, town);
                statement.bindString(19, region);
                //statement.execute();
                long entryID = statement.executeInsert();
                statement.clearBindings();
            }

            db.setTransactionSuccessful();
            db.endTransaction();

            SetTextLog("------ данные загружены");
//------------------------------------------------------------------------------------------------------------------------------------------------------------------

            SetTextLog("\n Загрузка актуальных анкет");
            JSONArray jsonParam_common2 = new JSONArray();
            JSONArray jArray_questionnaires = new JSONArray();
            JSONObject jsonObject_common_buffer2 = new JSONObject();
            JSONObject rootObject2 = new JSONObject();
            JSONObject oneObject2 = new JSONObject();

            jsonObject_common_buffer2.put("uuid", uuid);
            jsonObject_common_buffer2.put("process", "JSON");
            jsonObject_common_buffer2.put("post_url", "http://" + address_server + "/sync/download/json/?get_questionnaires=1");
            jsonParam_common2.put(jsonObject_common_buffer2);

            rootObject2.put("common", jsonParam_common2);

            json_data = new PostImageToServerAsync().execute(rootObject2).get();
            if (json_data.equals("False")){
                SetTextLog("----- отсутствует соединение с сервером");
                Toast.makeText(getApplicationContext(), "Отсутствует соединение с сервером", Toast.LENGTH_LONG).show();
                return;
            }

            jObject = new JSONObject(json_data);
            jArray_questionnaires = jObject.getJSONArray("questionnaires");
            if (jArray_questionnaires.length() == 0) {
                SetTextLog("------ новых данных по анкетам нет");
            } else {
                SetTextLog("------ информация получена, обновляем");

                // удаляем уже отправленные
                db.execSQL("DELETE FROM questionnaires WHERE sync != '0';");
                    String sql2 = "INSERT OR REPLACE INTO questionnaires (" +
                            "rec_id, name, address, latitude, longitude, kateg_f, name_net, ul, " +
                            "inn, torgter, addressd, tip, s_i, s_n, formtorg, tel, contact, post," +
                            "kanal, sostav, sync, status) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

                    db.beginTransaction();
                    SQLiteStatement statement_q = db.compileStatement(sql2);

                    for (i = 0; i < jArray_questionnaires.length(); i++) {
                        oneObject = jArray_questionnaires.getJSONObject(i);

                        rec_id = oneObject.getString("rec_id");
                        name = oneObject.getString("name");
                        address = oneObject.getString("full_address");
                        latitude = oneObject.getString("latitude");
                        longitude = oneObject.getString("longitude");
                        kateg_f = oneObject.getString("kateg_f");
                        name_net = oneObject.getString("name_net");
                        ul = oneObject.getString("ul");
                        inn = oneObject.getString("inn");
                        torgter = oneObject.getString("torgter");
                        addressd = oneObject.getString("addressd");
                        tip = oneObject.getString("tip");
                        s_i = oneObject.getString("s_i");
                        s_n = oneObject.getString("s_n");
                        formtorg = oneObject.getString("formtorg");
                        tel = oneObject.getString("tel");
                        contact = oneObject.getString("contact");
                        post = oneObject.getString("post");
                        kanal = oneObject.getString("kanal");
                        sostav = oneObject.getString("sostav");

                        address = address.replace("'", "").replace("\"", "");

                        statement_q.bindString(1, rec_id);
                        statement_q.bindString(2, name);
                        statement_q.bindString(3, address);
                        statement_q.bindString(4, latitude);
                        statement_q.bindString(5, longitude);
                        statement_q.bindString(6, kateg_f);
                        statement_q.bindString(7, name_net);
                        statement_q.bindString(8, ul);
                        statement_q.bindString(9, inn);
                        statement_q.bindString(10, torgter);
                        statement_q.bindString(11, addressd);
                        statement_q.bindString(12, tip);
                        statement_q.bindString(13, s_i);
                        statement_q.bindString(14, s_n);
                        statement_q.bindString(15, formtorg);
                        statement_q.bindString(16, tel);
                        statement_q.bindString(17, contact);
                        statement_q.bindString(18, post);
                        statement_q.bindString(19, kanal);
                        statement_q.bindString(20, sostav);
                        statement_q.bindString(21, "1");    //sync
                        statement_q.bindString(22, "3");    //status

                        //statement.execute();
                        long entryID = statement_q.executeInsert();
                        statement_q.clearBindings();
                        SetTextLog("------ Анкета [ " + rec_id + " ]");
                    }
                    db.setTransactionSuccessful();
                    db.endTransaction();
                }
                } catch (JSONException jsonException) {
                    jsonException.printStackTrace();
                } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        SetTextLog("------ данные загружены");
    }
}


