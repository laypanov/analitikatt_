package com.example.myapplication;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.location.LocationManager.GPS_PROVIDER;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    Geocoder geocoder;

    private TextView frame_top_latitude;
    private TextView frame_top_longitude;
    private TextView frame_top_address;
    private Button buttonAnketaFull;
    private Button buttonAnketaQuick;
    private Marker marker = null;
    private String rec_id = null;
    private int status = 0;
    private String actyalRec_id = null;
    private String name = null;
    private double latitude;
    private double longitude;
    private View frame_top;

    private  StringBuilder sb = new StringBuilder();
    private  double sb_latitude;
    private  double sb_longitude;

    private LocationManager locationManager;
    private static final long MIN_TIME = 5;
    private static final float MIN_DISTANCE = 1000;
    private static float map_distance_markers;
    private boolean firststart = false;
    List<LatLng>  locationList;
    boolean isLogging;
    private Context mContext;
    LatLng currentLocation;

    @Override
    protected void onResume(){
        super.onResume();
        try{
            get_markers(currentLocation);
            if (marker != null) {
                clearMarkerSet();
                return;
            }


        } catch (Exception e) {
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mContext = MapsActivity.this;

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        geocoder = new Geocoder(this, Locale.getDefault());

        this.frame_top_latitude = (TextView) this.findViewById(R.id.frame_top_latitude);
        this.frame_top_longitude = (TextView) this.findViewById(R.id.frame_top_longitude);
        this.frame_top_address = (TextView) this.findViewById(R.id.frame_top_address);
        this.frame_top = this.findViewById(R.id.frame_top);
        this.buttonAnketaFull = (Button) this.findViewById(R.id.buttonAnketaFull);
        this.buttonAnketaQuick = (Button) this.findViewById(R.id.buttonAnketaQuick);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 15000, 10,
                this);
        locationManager.requestLocationUpdates(GPS_PROVIDER,
                5000, 10, this);

        locationList = new ArrayList<>();
        isLogging = false;    // кнопка трека
        CheckBox checkBox = findViewById(R.id.frame_top_track);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    isLogging = true;
                    locationList.clear();
                    SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);
                    Cursor query2 = db.rawQuery("SELECT latitude, longitude FROM gps_track;", null);

                    if (query2 != null) {
                        if (query2.moveToFirst()) {
                            do {
                                Double lat = query2.getDouble(0);
                                Double lt = query2.getDouble(1);
                                LatLng cl = new LatLng(lat, lt);
                                locationList.add(cl);
                            } while (query2.moveToNext());
                            drawPolyLineOnMap(locationList);
                        }
                    }
                    get_markers(currentLocation);
                }else{
                    mMap.clear();
                    isLogging = false;
                    get_markers(currentLocation);
                }
            }
        });
    }

    public void GetViewTitileTop(String rec, String sb, double latitude, double longitude, int s){
        rec_id = rec;
        status = s;
        frame_top.setVisibility(View.VISIBLE);

        frame_top_address.setText("Адрес: " + sb);
        frame_top_latitude.setText("Широта: " + String.format("%.6f", latitude));
        frame_top_longitude.setText("Долгота: " + String.format("%.6f", longitude));
        frame_top_latitude.setVisibility(View.VISIBLE);
        frame_top_longitude.setVisibility(View.VISIBLE);
        frame_top_address.setVisibility(View.VISIBLE);

        if (status == 1){
            buttonAnketaQuick.setVisibility(View.INVISIBLE);
            buttonAnketaFull.setVisibility(View.VISIBLE);
        }else {
            buttonAnketaFull.setVisibility(View.VISIBLE);
            buttonAnketaQuick.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);

        } else {
            Toast.makeText(this, "Нет доступа к геоданным", Toast.LENGTH_LONG).show();
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 103);
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            private Marker previousMarker = null;
            @Override
            public boolean onMarkerClick(Marker marker) {
                int status = 0;
                Cursor query;

                String rec_id = marker.getTitle();
                //String locAddress = marker.getTitle();
                String firstChars = "";
                String address = null;
                String name = null;
                String town = null;
                String region = null;
                double latitude = 0;
                double longitude = 0;
                Log.v("Краткий клик", String.valueOf(rec_id));

                SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);

                query = db.rawQuery(
                        "SELECT _id FROM tt_info " +
                                "WHERE rec_id = '" + rec_id + "';", null);
                if (query != null) {
                    if (query.moveToFirst()) {
                        status = 1;
                    }
                }

                query = db.rawQuery(
                        "SELECT status FROM questionnaires " +
                                "WHERE rec_id = '" + rec_id + "';", null);
                if (query != null) {
                    if (query.moveToFirst()) {
                        status = query.getInt(0);;
                    }
                }
                // если точка с БД
                if (status == 1) {
                    query = db.rawQuery("SELECT name, latitude, longitude, address, town, region  FROM tt_info WHERE rec_id = '" + rec_id + "';", null);

                    if (query != null) {
                        if (query.moveToFirst()) {
                            name = query.getString(0);
                            latitude = query.getDouble(1);
                            longitude = query.getDouble(2);
                            address = query.getString(3);
                            town = query.getString(4);
                            region = query.getString(5);
                        }
                        address = address + ", " + town + ", " + region;
                        GetViewTitileTop(rec_id, address, latitude, longitude, status);
                    }
                } else if (status == 2) {
                    address = null;
                    name = null;
                    latitude = 0;
                    longitude = 0;

                    Cursor query2 = db.rawQuery("SELECT name, latitude, longitude, address  FROM questionnaires WHERE rec_id = '" + rec_id + "';", null);

                    if (query2 != null) {
                        if (query2.moveToFirst()) {
                            name = query2.getString(0);
                            latitude = query2.getDouble(1);
                            longitude = query2.getDouble(2);
                            address = query2.getString(3);
                        }
                        Log.v("address, latitude, longitude, name", address + latitude + longitude + name);
                        GetViewTitileTop(rec_id, address, latitude, longitude, status);
                    }
                } else  if (status == 3) {
                    Cursor query2 = db.rawQuery("SELECT name, latitude, longitude, address  FROM questionnaires WHERE rec_id = '" + rec_id + "';", null);

                    if (query2 != null) {
                        if (query2.moveToFirst()) {
                            name = query2.getString(0);
                            latitude = query2.getDouble(1);
                            longitude = query2.getDouble(2);
                            address = query2.getString(3);
                        }
                        Log.v("address, latitude, longitude, name", address + latitude + longitude + name);
                        GetViewTitileTop(rec_id, address, latitude, longitude, status);
                    }

                } else if (status == 0) {
                    Log.v("MAP_ЕСЛИ НОВАЯ ТТ", String.valueOf(firstChars));
                    rec_id = null;
                    Log.v("NAME_MAP----->", String.valueOf(name));
                    GetViewTitileTop("", sb.toString(), sb_latitude, sb_longitude, status);
                }
                db.close();
                return true;
            }
        });

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng point) {
                //если отщелкнули маркер
                if (marker != null) {
                    clearMarkerSet();
                    return;
                }

                LatLng latLng = point;

                List<Address> addresses = new ArrayList<>();
                try {
                    addresses = geocoder.getFromLocation(point.latitude, point.longitude,1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    android.location.Address address = addresses.get(0);

                    if (address != null) {

                        for (int i = 0; i < 1; i++){
                            sb.append(address.getAddressLine(i));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    sb.append("Нет информации");
                }

                marker = mMap.addMarker(new MarkerOptions().position(point).title("ТТ")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                sb_latitude = point.latitude;
                sb_longitude = point.longitude;

                Log.v("Долгий клик", String.valueOf(sb));
                int status = 0;
                GetViewTitileTop ("", sb.toString(), sb_latitude, sb_longitude, status);
            }

        });
    }

    public void clearMarkerSet(){
        marker.remove();
        marker = null;
        rec_id = null;
        frame_top_latitude.setVisibility(View.INVISIBLE);
        frame_top_longitude.setVisibility(View.INVISIBLE);
        frame_top_address.setVisibility(View.INVISIBLE);
        buttonAnketaFull.setVisibility(View.INVISIBLE);
        buttonAnketaQuick.setVisibility(View.INVISIBLE);
        //очищаем
        sb.setLength(0);
    }

    // Метод обработки нажатия на кнопку
    public void buttonAnketaQuick(View view) {
        if (rec_id!=null){
            intentToAnketa("Quick");
        }
    }

    // Метод обработки нажатия на кнопку
    public void buttonAnketaFull(View view) {
        if (rec_id!=null){
            intentToAnketa("Full");
        }
    }

    public void intentToAnketa(String val){
        Intent intent = null;
        if (val.equals("Quick")) {
            intent = new Intent(this, AnketaTTQuickActivity.class);
        }else if (val.equals("Full")){
            intent = new Intent(this, AnketaTTFullActivity.class);
        }

        intent.putExtra("frame_top_address", frame_top_address.getText().toString().replace("Адрес: ", ""));
        intent.putExtra("frame_top_latitude", frame_top_latitude.getText().toString().replace("Широта: ", ""));
        intent.putExtra("frame_top_longitude", frame_top_longitude.getText().toString().replace("Долгота: ", ""));
        intent.putExtra("rec_id", rec_id);
        intent.putExtra("status", status);
        intent.putExtra("map", true);
        startActivity(intent);
    }

    public float getDistance(LatLng my_latlong, LatLng frnd_latlong) {
        Location l1 = new Location("One");
        l1.setLatitude(my_latlong.latitude);
        l1.setLongitude(my_latlong.longitude);

        Location l2 = new Location("Two");
        l2.setLatitude(frnd_latlong.latitude);
        l2.setLongitude(frnd_latlong.longitude);

        float distance = l1.distanceTo(l2);
        return distance;
    }


    public void get_markers(LatLng l){
        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);

        Cursor query0 = db.rawQuery("SELECT map_distance_markers FROM options;", null);
        if (query0.moveToFirst()) {
            map_distance_markers = query0.getInt(0);
        }
        query0.close();


        Cursor query;
        query = db.rawQuery("SELECT rec_id, name, latitude, longitude, address  FROM tt_info;", null);
        if (query != null) {
            if (query.moveToFirst()) {
                do {
                    String rec_id = query.getString(0);
                    String name = query.getString(1);
                    Double latitude = query.getDouble(2);
                    Double longitude = query.getDouble(3);
                    //address = query.getString(4);
                    LatLng l2 = new LatLng(latitude, longitude);

                    Log.v("getDistance", String.valueOf(map_distance_markers));
                    if (getDistance(l, l2) <= map_distance_markers + .0f){
                        Log.v("getDistance", String.valueOf("11111111111111"));
                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(latitude, longitude))
                                .title(rec_id)
                                .snippet(name));
                    }
                } while (query.moveToNext());
            }
        }
        query.close();

        query = db.rawQuery("SELECT rec_id, name, latitude, longitude, status FROM questionnaires WHERE sync != 'null'", null);
        if (query != null) {
            if (query.moveToFirst()) {
                do {
                    String rec_id = query.getString(0);
                    String name = query.getString(1);
                    Double latitude = query.getDouble(2);
                    Double longitude = query.getDouble(3);
                    int status = query.getInt(4);
                    LatLng l2 = new LatLng(latitude, longitude);

                    if (getDistance(l, l2) <= map_distance_markers + .0f) {

                        if (status == 2) {
                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(latitude, longitude))
                                    .title(rec_id)
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                                    .snippet(name));
                        }else if (status == 3){
                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(latitude, longitude))
                                    .title(rec_id)
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))
                                    .snippet(name));
                        }
                    }
                } while (query.moveToNext());
            }
        }
        query.close();
        db.close();
    }


    @Override
    public void onLocationChanged(Location location) {
        currentLocation = new LatLng(location.getLatitude(), location.getLongitude());

        if (!firststart) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16));
            firststart = true;
        }else{
            if(isLogging){
                locationList.add(currentLocation);
                drawPolyLineOnMap(locationList);
            }
        }

        SQLiteDatabase db = getBaseContext().openOrCreateDatabase("analitika_tt.db", MODE_PRIVATE, null);

        db.execSQL("INSERT INTO gps_track (latitude, longitude, date_created) VALUES (" +
                "'" + location.getLatitude() + "', '" + location.getLongitude() + "', 'datetime()');");

        get_markers(currentLocation);
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }


    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    public void drawPolyLineOnMap(List<LatLng> list) {
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(Color.DKGRAY);
        polyOptions.width(5);
        polyOptions.addAll(list);

        mMap.clear();
        mMap.addPolyline(polyOptions);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : list) {
            builder.include(latLng);
        }

        final LatLngBounds bounds = builder.build();
    }
}